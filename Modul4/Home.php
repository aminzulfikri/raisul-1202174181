<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-light" style="background-color: #ea5ffa;">
		<a class="navbar-brand" href="home.php"><img src="EAD.png" width="200"></a>
	    <ul class="nav justify-content-end">
	    	<?php if (isset($_SESSION["id"])) {?>
	    		<li class="nav-item dropdown active">
		    	<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		        	<a class="dropdown-item" href="Profile.php">Profile</a>
		        	<a class="dropdown-item" href="Cart.php">Cart</a>
		          	<div class="dropdown-divider"></div>
		          	<a class="dropdown-item" href="crud.php?logout=out">Log Out</a>
		        </div>
	     	</li>
	    	<?php } else {?>
	    		<li class="nav-item active">
	        		<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#modalLogin">Login</a>
	      		</li>
	      		<li class="nav-item active">
	        		<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#modalRegister">Register</a>
	      		</li>
	    	<?php } ?>
	    </ul>
	</nav>
	<div class="card my-3 mx-auto text-white" style="width: 78%; background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,21,121,1) 18%, rgba(121,8,133,1) 30%, rgba(144,66,168,1) 51%, rgba(255,0,254,1) 75%);">
	  	<div class="card-body">
	    	<span class="align-middle">
	    		<h1>
	    		Hello Coders!
	    		</h1>
	    		<p>
	    		Welcome to our store,  please a look for the product you might buy.
	    		</p>
	    	</span>
	  	</div>
	</div>
	<div class="row mx-auto" style="width: 70%;">
  		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 28rem;">
			  	<img src="https://www.dicoding.com/images/original/academy/belajar_membuat_aplikasi_android_untuk_pemula_logo_070119140911.jpg" class="card-img-top" alt="...">
			  	<div class="card-body" style="height: 20rem;">
			    	<h5 class="card-title">
			    	Belajar Android Untuk Pemula
			    	</h5>
			    	<p class="card-text">
			    		<b>Rp.250.000,-</b>
			    	</p>
			    	<p class="card-text">
			    	Android semakin digandrungi. Per Maret 2018 ada lebih dari 3,6 juta aplikasi Android di Google Play Store (Statista).
			    	</p>
			  	</div>
			  	<div class="card-body">
			    	<a href="crud.php?buy=buy&product=Belajar+Android+Untuk+Pemula&price=250000" class="card-link btn btn-primary" role="button" style="width: 25rem;">Buy</a>
			  	</div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 28rem;">
			  	<img src="https://www.dicoding.com/images/original/academy/menjadi_android_developer_expert_logo_070119140352.jpg" class="card-img-top" alt="...">
			  	<div class="card-body" style="height: 20rem;">
			    	<h5 class="card-title">
			    	Menjadi Android Developer Expert
			    	</h5>
			    	<p class="card-text">
			    		<b>Rp.500.000,-</b>
			    	</p>
			    	<p class="card-text">
			    	Jadilah expert di dunia pemrograman Android. Materi disusun oleh EAD sebagai Google Authorized Training Partner.
			   		</p>
			  	</div>
			  	<div class="card-body">
			    	<a href="crud.php?buy=buy&product=Menjadi+Android+Developer+Expert&price=500000" class="card-link btn btn-primary" role="button" style="width: 25rem;">Buy
			    	</a>
			  	</div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 28rem;">
			  	<img src="https://www.dicoding.com/images/original/academy/belajar_android_jetpack_pro_logo_020619220133.jpg" class="card-img-top" alt="...">
			  	<div class="card-body" style="height: 20rem;">
			    	<h5 class="card-title">
			    	Belajar Android Jetpack Pro
			    	</h5>
			    	<p class="card-text">
			    		<b>Rp.1.000.000,-</b>
			    	</p>
			    	<p class="card-text">
			    	Mari belajar Jetpack, tools yang bisa bikin level-up skill Android-mu.
			    	</p>
			  	</div>
			  	<div class="card-body">
			    	<a href="crud.php?buy=buy&product=Belajar+Android+Jetpack+Pro&price=1000000" class="card-link btn btn-primary" role="button" style="width: 25rem;">Buy
			    	</a>
			  	</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<form method="post" action="crud.php">
	      			<div class="modal-header">
	        			<h5 class="modal-title" id="loginModalLabel">Login
	        			</h5>
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          					<span aria-hidden="true">&times;</span>
        				</button>
	      			</div>
	      			<div class="modal-body">
			        	<div class="form-group">
			            	<label for="recipient-email" class="col-form-label">Email Address</label>
			            	<input type="email" class="form-control" name="nmEmail" placeholder="Enter Email" required>
			          	</div>
			          	<div class="form-group">
							<label for="recipient-password" class="col-form-label">Password</label>
							<input type="password" class="form-control" name="nmPassword" placeholder="Enter Password" required>
			          	</div>
	      			</div>
	      			<div class="modal-footer">
	        			<input type="button" class="btn btn-danger" data-dismiss="modal" value="Close">
	        			<input type="submit" class="btn btn-success" value="Login" name="login">
	      			</div>
	  	  		</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<form method="post" action="crud.php">
	      			<div class="modal-header">
	        			<h5 class="modal-title" id="registerModalLabel">Register
	        			</h5>
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          				<span aria-hidden="true">&times;</span>
	        			</button>
	      			</div>
	      			<div class="modal-body">
	          			<div class="form-group">
	            			<label for="recipient-email" class="col-form-label">Email Address</label>
	            			<input type="email" class="form-control" name="registEmail" placeholder="Enter Email" required>
	          			</div>
	          			<div class="form-group">
	            			<label for="recipient-username" class="col-form-label">Username</label>
	            			<input type="text" class="form-control" name="registUsername" placeholder="Enter Username" required>
	          			</div>
	          			<div class="form-group">
	            			<label for="recipient-password" class="col-form-label">Password</label>
	            			<input type="password" class="form-control" name="registPassword" placeholder="Password" required>
	          			</div>
	          			<div class="form-group">
	            			<label for="message-repassword" class="col-form-label">Confirm Password</label>
	            			<input type="password" class="form-control" name="registConPassword" placeholder="Confirm Password" required>
	          			</div>
	      			</div>
	      			<div class="modal-footer">
	        			<input type="button" class="btn btn-danger" data-dismiss="modal" value="Close">
	        			<input type="submit" class="btn btn-success" value="Register" name="register">
	      			</div>
	  	  		</form>
	    	</div>
	  	</div>
	</div>
	<footer class="footer">
		<div class="container">
	    	<div class="footer text-center py-3">
	    	© EAD STORE
	    	</div>
      	</div>
    </footer>
</body>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>