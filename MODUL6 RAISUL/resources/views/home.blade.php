@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($posts as $isi)
        <div class="col-md-12">
            <div class="card mx-auto my-2" style="width: 600px;">
                <div class="card-header">
                    <img src="{{asset($isi->user->avatar)}}" width="50" style="border-radius: 50%; border: solid 1;">
                    {{$isi->user->name}}
                </div>

                <div class="card-body">
                    <center>
                        <a href="{{ route('post.show', $isi->id) }}">
                        <img src="{{$isi->image}}" width="500">
                        </a>
                    </center>
                </div>
                <div class="card-footer">
                    <a href="{{ url('likes/'.$isi->id) }}">
                        <i class="fa fa-heart-o" style="font-size:24px; color: black;"></i>
                    </a>
                    <i class="fa far fa-comment-o" style="font-size:24px; color: black;"></i>
                    <b>{{$isi->likes}} Like</b>
                    <p><b>{{$isi->user->email}}</b>
                    {{$isi->caption}}
                    </p>
                    <p>
                        @foreach($komentars as $komentar)
                            @if($komentar->post_id == $isi->id)
                                <b>{{$komentar->email}}</b> {{$komentar->comment}}<br>
                            @endif
                        @endforeach
                    </p>
                    <form action="{{route('komentar')}}" method="POST">  
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="post_id" value="{{$isi->id}}">
                            <input type="text" class="form-control" name="komentar" id="detail_komentar" required placeholder="Add a comment..">
                            <button type="submit" class="btn btn-primary" name="create_komentar" value="kirim">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
