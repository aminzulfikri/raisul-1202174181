<?php 
	$total = 0;

	if (!empty($_POST['es1'])) {
		$total += $_POST['es1'];
	}
	if (!empty($_POST['es2'])) {
		$total += $_POST['es2'];
	}
	if (!empty($_POST['es3'])) {
		$total += $_POST['es3'];
	}
	if (!empty($_POST['es4'])) {
		$total += $_POST['es4'];
	}
	if (!empty($_POST['es5'])) {
		$total += $_POST['es5'];
	}

	if ($_POST['member'] == "Ya") {
		$total *= 0.9;
	}
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Nota</title>
 	<style type="text/css">
 		th{
 			padding-right: 20px;
 		}
 	</style>
 </head>
 <body style="background-color: lightblue">
 	<h1 style="text-align: center">Transaksi Pemesanan</h1>
 	<br>
 	<p style="font-family: Century Gothic; text-align: center">
 		Terima kasih telah berbelanja dengan Kopi Susu Duarrr!
 	</p>
 	<h1 style="text-align: center">Rp. <?php echo $total; ?>.00,-</h1>
 	<br>
 	<table style="text-align: left; line-height: 200%" align="center">
 		<tr>
 			<th>ID</th>
 			<td>
 				<?php echo $_POST['order']; ?>
 			</td>
 		</tr>
 		<tr>
 			<th>Nama</th>
 			<td><?php echo $_POST['nama']; ?></td>
 		</tr>
 		<tr>
 			<th>Email</th>
 			<td><?php echo $_POST['email']; ?></td>
 		</tr>
 		<tr>
 			<th>Alamat</th>
 			<td><?php echo $_POST['alamat']; ?></td>
 		</tr>
 		<tr>
 			<th>Member</th>
 			<td><?php echo $_POST['member']; ?></td>
 		</tr>
 		<tr>
 			<th>Pembayaran</th>
 			<td><?php echo $_POST['metode']; ?></td>
 		</tr>
 	</table>
 </body>
 </html>