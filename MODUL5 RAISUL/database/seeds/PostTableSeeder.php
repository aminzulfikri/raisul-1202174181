<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Str;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Kucheng bengal', 
            'image' => 'Bengal.jpeg', 
        ]); 
        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Kucheng maine coon', 
            'image' => 'maine_coon.jpg', 
        ]); 
        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Kucheng ragdoll', 
            'image' => 'ragdoll.jpg', 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Kucheng munchkin', 
            'image' => 'munchkin.jpg', 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Kucheng scottish fold', 
            'image' => 'scottish_fold.jpg', 
        ]);
    }
}
