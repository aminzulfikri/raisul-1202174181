@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($post as $isi)
        <div class="col-md-12">
            <div class="card mx-auto my-2" style="width: 600px;">
                <div class="card-header">
                    <img src="raisul.jpg" width="50" style="border-radius: 50%; border: solid 1;">
                    {{Auth::user()->name}}
                </div>

                <div class="card-body">
                    <center>
                        <img src="{{$isi->image}}" width="500">
                    </center>
                </div>
                <div class="card-footer">
                    <b>{{Auth::user()->email}}</b><br>
                    {{$isi->caption}}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
